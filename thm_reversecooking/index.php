<?php  
/*------------------------------------------------------------------------
* @version     1.1.0
 * @package     com_thm_reverscookings
 * @copyright   Copyright (C) 2012. All rights reserved.
 * @license     GNU General Public License 
 *  @author Bassing <dominik.bassing@mni.thm.de>
 *  @author Seefeldt <Stefan.Seefeldt@mni.thm.de>
 *  @author Schneider <stefan.schneider@mni.thm.de>
 *  @author Omoko <guy.bertrand.omoko@hotmail.com>
 *  @author Noubeva <guylene.deutcho.noubevam@mni.thm.de>
 *  @author Timma<dieudonne.timma.meyatchie@mni.thm.de> 
* copyright Copyright © 2011 example.com. All rights reserved.
* @license  http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
* Website   http://www.example.com
-------------------------------------------------------------------------*/

defined( '_JEXEC' ) or die;

// variables
$app = JFactory::getApplication();
$doc = JFactory::getDocument();
$params = $app->getParams();
$pageclass = $params->get('pageclass_sfx');
$tpath = $this->baseurl.'/templates/'.$this->template;

$this->setGenerator(null);

// load sheets and scripts
$doc->addStyleSheet($tpath.'/css/template.css.php?v=1.0.0');
$doc->addScript($tpath.'/js/modernizr.js'); // <- this script must be in the head

// unset scripts, put them into /js/template.js.php to minify http requests
unset($doc->_scripts[$this->baseurl.'/media/system/js/mootools-more.js']);
unset($doc->_scripts[$this->baseurl.'/media/system/js/core.js']);
unset($doc->_scripts[$this->baseurl.'/media/system/js/caption.js']);

?>
<!doctype html>
<!--[if IEMobile]><html class="iemobile" lang="<?php echo $this->language; ?>"> <![endif]-->
<!--[if lt IE 7]> <html class="no-js ie6 oldie" lang="<?php echo $this->language; ?>"> <![endif]-->
<!--[if IE 7]>    <html class="no-js ie7 oldie" lang="<?php echo $this->language; ?>"> <![endif]-->
<!--[if IE 8]>    <html class="no-js ie8 oldie" lang="<?php echo $this->language; ?>"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js"
	lang="<?php echo $this->language; ?>">
<!--<![endif]-->

<head>
<script type="text/javascript"
	src="<?php echo $tpath.'/js/template.js.php'; ?>"></script>
<jdoc:include type="head" />
<meta name="viewport"
	content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
<!-- mobile viewport -->
<link rel="stylesheet" media="only screen and (max-width: 768px)"
	href="<?php echo $tpath; ?>/css/tablet.css" type="text/css" />
<link rel="stylesheet"
	media="only screen and (min-width: 320px) and (max-width: 480px)"
	href="<?php echo $tpath; ?>/css/phone.css" type="text/css" />
<!--[if IEMobile]><link rel="stylesheet" media="screen" href="<?php echo $tpath; ?>/css/phone.css" type="text/css" /><![endif]-->
<!-- iemobile -->
<link rel="apple-touch-icon-precomposed"
	href="<?php echo $tpath; ?>/apple-touch-icon-57x57.png">
<!-- iphone, ipod, android -->
<link rel="apple-touch-icon-precomposed" sizes="72x72"
	href="<?php echo $tpath; ?>/apple-touch-icon-72x72.png">
<!-- ipad -->
<link rel="apple-touch-icon-precomposed" sizes="114x114"
	href="<?php echo $tpath; ?>/apple-touch-icon-114x114.png">
<!-- iphone retina -->
<!--[if lte IE 8]>
    <style> 
      {behavior:url(<?php echo $tpath; ?>/js/PIE.htc);}
    </style>
  <![endif]-->
</head>

<body class="<?php echo $pageclass; ?>">
	<div id="overall">
		<div id="header">
			<div class="inheader">
				<div class="row">
					<div class="pic span12">
						<div id="pic">
							
						</div>
					</div>
					
				</div>
			</div>
		</div>
		<div id="main">
			<div class="inmain">
				<div class="row">
				</div>
				<div class="navbar span12">
						<div class="navbar-inner">
							<div class="container">

								<!-- .btn-navbar is used to toogle  collapsed navbar content -->
								<a class="btn btn-navbar" data-toggle="collapse"
									data-target=".nav-collapse"> <span class="icon-bar"></span> <span
									class="icon-bar"></span> <span class="icon-bar"></span>
								</a>

								<!-- Be sure to leave the brand out there if you want it shown -->
								<a class="brand" href="<?php echo $this->baseurl; ?>/"><?php echo $app->getCfg('sitename'); ?></a>

								<!-- Everything you want hidden at 940px or less, place within here -->
								<div class="nav-collapse">
									<div class="pull-left visible-desktop">
										<jdoc:include type="modules" name="menu" />
									</div>
									<div class="insearch pull-right visible-desktop">
										<jdoc:include type="modules" name="search" />
									</div>
									<div class="hidden-desktop">
										<jdoc:include type="modules" name="menu" />
										<jdoc:include type="modules" name="search" />
									</div>
								</div>

							</div>
						</div>
					</div>
				<div class="span12">
					<jdoc:include type="modules" name="breadcrumbs" />
				</div>
				<div id="content" class="span8">
					<div class="incontent">
						<jdoc:include type="message" />
						<jdoc:include type="component" />
					</div>
				</div>
				<div id="sidebar" class="span4">
					<div class="insidbar">
						<jdoc:include type="modules" name="sidebar" style="xhtml" />
					</div>
				</div>
			</div>
		</div>
		<!-- ***** FOOTER ***** -->
		<div class="container">
			<div class="row">
				<div class="span12">
					<p>
						Copyright &copy;
						<?php echo date('Y'); ?>
						-
						<?php echo $app->getCfg('sitename'); ?>
					</p>
				</div>
			</div>
		</div>
	</div>
	<jdoc:include
		type="modules" name="debug" />

	<script type="text/javascript">
  (function($){   
    $(document).ready(function(){
      // dropdown
      $('.item-102').addClass('dropdown');
      $('.item-102 > a').addClass('dropdown-toggle');
      $('.item-102 > a').attr('data-toggle', 'dropdown');
      $('.item-102 > a').append('<b class="caret"></b>');
      $('.item-102 > ul').addClass('dropdown-menu');
      // divider 
      $('<li class="divider"></li>').insertAfter('.item-105');
    });
  })(jQuery);
</script>
</body>

</html>

